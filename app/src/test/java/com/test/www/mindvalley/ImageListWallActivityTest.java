package com.test.www.mindvalley;

import com.test.www.mindvalley.activity.SplashActivity;
import com.test.www.mindvalley.activity.ImageListWallActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by dharmaraj on 14/07/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, packageName = "com.test.www.mindvalley")
public class ImageListWallActivityTest extends SplashActivity
{
    private SplashActivity splashActivity;
    private ImageListWallActivity imageListWallActivity;

    @Before
    public void setUp()
    {
        imageListWallActivity = Robolectric.buildActivity(ImageListWallActivity.class).create().get();
        splashActivity = Robolectric.buildActivity(SplashActivity.class).create().get();
    }

    @Test
    public void checkActivityNotNull() throws Exception
    {
        assertNotNull(splashActivity);
        assertNotNull(imageListWallActivity);
    }
}
