package com.test.www.mindvalley.network.service;

import com.test.www.mindvalley.network.userlisting.model.ImageWallListingResponseNetworkModel;

import java.util.List;

import retrofit.http.GET;
import rx.Observable;
/**
 * Created by dharmaraj on 14/07/2016.
 */

public interface ImageListWallService
{
    @GET("/raw/wgkJgazE")
    Observable<List<ImageWallListingResponseNetworkModel>> getImageListingWallData();
}

