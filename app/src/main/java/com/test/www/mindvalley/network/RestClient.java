package com.test.www.mindvalley.network;

import android.content.Context;

import com.mindvalley.common.framework.BuildConfig;
import com.mindvalley.common.framework.network.callback.IViewCallback;
import com.mindvalley.common.framework.network.rest.AbstractRestClient;
import com.mindvalley.common.framework.util.LogUtil;
import com.test.www.mindvalley.mapper.ImageListWallViewMapper;
import com.test.www.mindvalley.network.service.ImageListWallService;
import com.test.www.mindvalley.network.userlisting.model.ImageWallListingResponseNetworkModel;
import com.test.www.mindvalley.util.ApiUtil;
import com.test.www.mindvalley.viewmodel.userlisting.UserListingListViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
/**
 * Created by dharmaraj on 14/07/2016.
 */
public class RestClient extends AbstractRestClient
{
    private ImageListWallService imageListWallService;


    public RestClient(Context context)
    {
        super(context, ApiUtil.BASE_URL, BuildConfig.DEBUG);
    }

    public RestClient(Context context, String URL)
    {
        super(context, URL, BuildConfig.DEBUG);
    }

    @Override
    public void initApi()
    {

        imageListWallService =  restAdapter.create(ImageListWallService.class);

    }

    public ImageListWallService getImageListWallService()
    {
        return imageListWallService;
    }



    public void getUserListingData( final IViewCallback<UserListingListViewModel> pViewCallback)
    {
        getImageListWallService().getImageListingWallData()
                                 .subscribeOn(Schedulers.newThread())
                                 .observeOn(AndroidSchedulers.mainThread())
                                 .subscribe(new Subscriber<List<ImageWallListingResponseNetworkModel>>() {
                   @Override
                   public final void onCompleted() {
                       // do nothing
                   }

                   @Override
                   public final void onError(Throwable e) {
                       LogUtil.printLog("Error", e.getMessage());
                   }

                   @Override
                   public final void onNext(List<ImageWallListingResponseNetworkModel> response) {
                       UserListingListViewModel userListingListViewModel= new ImageListWallViewMapper().toViewModel(response);
                       pViewCallback.onSuccess(userListingListViewModel);
                   }
               });
    }

}
