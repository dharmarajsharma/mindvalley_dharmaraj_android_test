package com.test.www.mindvalley.application;

import android.content.Context;
import android.content.Intent;
import com.mindvalley.common.framework.application.BaseApplication;
import com.mindvalley.common.framework.navigation.Navigator;
import com.test.www.mindvalley.activity.ImageListWallActivity;
import com.test.www.mindvalley.network.RestClient;
/**
 * Created by dharmaraj on 14/07/2016.
 */
public class ImageListWallApplication extends BaseApplication
{

    public static RestClient restClient;

    public static RestClient getRestClient()
    {
        return restClient;
    }

    @Override
    public void onPostCreate()
    {
        restClient = new RestClient(getApplicationContext());
    }



    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
    }

    @Override
    public Navigator getNavigator()
    {
        return new Navigator()
        {
            @Override
            public void launchUserListingActivity(Context context)
            {
                Intent intent = new Intent(context, ImageListWallActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }

        };
    }
}
