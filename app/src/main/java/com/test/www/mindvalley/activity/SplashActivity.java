package com.test.www.mindvalley.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.widget.ImageView;

import com.mindvalley.common.framework.activity.BaseActivity;
import com.mindvalley.common.framework.util.AnimationUtil;
import com.test.www.mindvalley.R;


/**
 * Created by dharmaraj on 14/07/2016.
 */
public class SplashActivity extends BaseActivity
{
    // Splash screen timer
    private static final int SPLASH_TIME_OUT = 3000;
    private static final int ANIMATION_ONE_CYCLE = 915;

    private ImageView animationView;

    @Override
    protected void onPreSetContentView()
    {
        //do nothing
    }

    @Override
    protected void onActivityReady()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                playAnimation();
            }
        }, 300);
    }

    private void playAnimation()
    {
        CountDownTimer timer = new CountDownTimer(SPLASH_TIME_OUT, ANIMATION_ONE_CYCLE)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    //if we have Animatable drawable than we can start animation here.
                }
            }

            @Override
            public void onFinish()
            {

                getNavigator().launchUserListingActivity(SplashActivity.this);
                AnimationUtil.setTransactionAnimationFromCallerActivity(SplashActivity.this);
                // close this activity
                finish();
            }
        };

        timer.start();
    }

    @Override
    protected int getActivityLayout()
    {
        return R.layout.activity_splash;
    }

    @Override
    public String getScreenName()
    {
        return "Splash Screen";
    }

    @Override
    protected void initViews()
    {
        //initialise views here

        animationView = (ImageView) findViewById(R.id.vectorAnimation);
    }
}
