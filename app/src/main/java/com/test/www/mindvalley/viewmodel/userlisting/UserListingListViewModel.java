package com.test.www.mindvalley.viewmodel.userlisting;

import com.mindvalley.common.framework.viewmodel.IViewModel;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by dharmaraj on 14/07/2016.
 */
public class UserListingListViewModel implements IViewModel
{
    List<ImageWallListingViewModel> imageWallListingViewModels = new ArrayList<>();

    public List<ImageWallListingViewModel> getImageWallListingViewModels()
    {
        return imageWallListingViewModels;
    }

    public void setImageWallListingViewModels(List<ImageWallListingViewModel> imageWallListingViewModels)
    {
        this.imageWallListingViewModels = imageWallListingViewModels;
    }
}
