package com.test.www.mindvalley.mapper;

import com.mindvalley.common.framework.mapper.IMapper;
import com.test.www.mindvalley.network.userlisting.model.ImageWallListingResponseNetworkModel;
import com.test.www.mindvalley.viewmodel.userlisting.UserListingListViewModel;
import com.test.www.mindvalley.viewmodel.userlisting.ImageWallListingViewModel;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by dharmaraj on 14/07/2016.
 */
public class ImageListWallViewMapper implements IMapper<List<ImageWallListingResponseNetworkModel>, UserListingListViewModel>
{
    UserListingListViewModel viewModel;

    @Override
    public UserListingListViewModel toViewModel(List<ImageWallListingResponseNetworkModel> imageWallListingResponseNetworkModelList)
    {
        viewModel = new UserListingListViewModel();
        if(imageWallListingResponseNetworkModelList == null && imageWallListingResponseNetworkModelList.size() == 0)
        {
            return viewModel;
        }
        List<ImageWallListingViewModel> imageWallListingViewModels = new ArrayList<>();
        for(ImageWallListingResponseNetworkModel imageWallListingResponseNetworkModel : imageWallListingResponseNetworkModelList)
        {
            ImageWallListingViewModel imageWallListingViewModel = new ImageWallListingViewModel();
            imageWallListingViewModel.setCategories(imageWallListingResponseNetworkModel.getCategories());
            imageWallListingViewModel.setColor(imageWallListingResponseNetworkModel.getColor());
            imageWallListingViewModel.setCreatedAt(imageWallListingResponseNetworkModel.getCreatedAt());
            imageWallListingViewModel.setCurrentUserCollections(imageWallListingResponseNetworkModel.getCurrentUserCollections());
            imageWallListingViewModel.setHeight(imageWallListingResponseNetworkModel.getHeight());
            imageWallListingViewModel.setId(imageWallListingResponseNetworkModel.getId());
            imageWallListingViewModel.setLikedByUser(imageWallListingResponseNetworkModel.getLikedByUser());
            imageWallListingViewModel.setUser(imageWallListingResponseNetworkModel.getUser());
            imageWallListingViewModel.setLikes(imageWallListingResponseNetworkModel.getLikes());
            imageWallListingViewModel.setLinks(imageWallListingResponseNetworkModel.getLinks());
            imageWallListingViewModel.setUrls(imageWallListingResponseNetworkModel.getUrls());
            imageWallListingViewModel.setWidth(imageWallListingResponseNetworkModel.getWidth());
            imageWallListingViewModels.add(imageWallListingViewModel);
        }
        viewModel.setImageWallListingViewModels(imageWallListingViewModels);
        return viewModel;
    }
}