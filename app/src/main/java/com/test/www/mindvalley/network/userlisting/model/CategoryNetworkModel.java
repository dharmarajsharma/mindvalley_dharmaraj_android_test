package com.test.www.mindvalley.network.userlisting.model;

/**
 * Created by dharmaraj on 13/07/2016.
 */

public class CategoryNetworkModel
{
    private Integer id;
    private String title;
    private Integer photoCount;
    private CategoryLinksNetworkModel links;

    /**
     * @return The id
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id
     *         The id
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *         The title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return The photoCount
     */
    public Integer getPhotoCount()
    {
        return photoCount;
    }

    /**
     * @param photoCount
     *         The photo_count
     */
    public void setPhotoCount(Integer photoCount)
    {
        this.photoCount = photoCount;
    }

    /**
     * @return The links
     */
    public CategoryLinksNetworkModel getLinks()
    {
        return links;
    }

    /**
     * @param links
     *         The links
     */
    public void setLinks(CategoryLinksNetworkModel links)
    {
        this.links = links;
    }
}
