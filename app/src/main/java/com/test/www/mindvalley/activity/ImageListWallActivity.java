package com.test.www.mindvalley.activity;

import android.graphics.Bitmap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;
import com.mindvalley.common.framework.activity.BaseActivity;
import com.mindvalley.common.framework.application.BaseApplication;
import com.mindvalley.common.framework.image.ImageDisplayOption;
import com.mindvalley.common.framework.image.listener.AbstractImageLoadingListener;
import com.mindvalley.common.framework.network.callback.IViewCallback;
import com.mindvalley.common.framework.util.LogUtil;
import com.mindvalley.common.framework.view.CircleImageView;
import com.mindvalley.common.framework.view.animator.ScaleInRecyclerViewAdapter;
import com.test.www.mindvalley.R;
import com.test.www.mindvalley.application.ImageListWallApplication;
import com.test.www.mindvalley.viewmodel.userlisting.UserListingListViewModel;
import com.test.www.mindvalley.viewmodel.userlisting.ImageWallListingViewModel;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.util.TimeoutController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.RetrofitError;
/**
 * Created by dharmaraj on 14/07/2016.
 */

public class ImageListWallActivity extends BaseActivity
{

    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView recyclerViewImageWall;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageListWallAdapter imageListWallAdapter;

    @Override
    protected void onPreSetContentView()
    {

    }

    @Override
    protected void initViews()
    {
        //initialise views here

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        recyclerViewImageWall = (RecyclerView) findViewById(R.id.recyclerViewImageWall);
    }

    @Override
    protected void onActivityReady()
    {
        mLinearLayoutManager = new LinearLayoutManager(this);

        /**
         * Fetch data and set up in Android CardView/RecycleView
         */
        fetchUserListingData();

        // Set up swipe refresh layout.

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                                                    android.R.color.holo_orange_light, android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                fetchUserListingData();
            }
        });
    }

    @Override
    protected int getActivityLayout()
    {
        return R.layout.activity_main;
    }

    @Override
    public String getScreenName()
    {
        return "ImageListWallActivity";
    }

    public void fetchUserListingData()
    {
        mSwipeRefreshLayout.setRefreshing(true);
        ImageListWallApplication.getRestClient().getUserListingData(new IViewCallback<UserListingListViewModel>()
        {
            @Override
            public void onSuccess(UserListingListViewModel userListingListViewModel)
            {
                mSwipeRefreshLayout.setRefreshing(false);

                imageListWallAdapter = new ImageListWallAdapter(userListingListViewModel);
                // Set up recycler view for image list wall.
                recyclerViewImageWall.setNestedScrollingEnabled(false);
                recyclerViewImageWall.setHasFixedSize(false);
                recyclerViewImageWall.setAdapter(new ScaleInRecyclerViewAdapter(imageListWallAdapter, true));

                mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerViewImageWall.setLayoutManager(mLinearLayoutManager);
            }

            @Override
            public void onFailure(Throwable error)
            {
                mSwipeRefreshLayout.setRefreshing(false);
                if(error instanceof TimeoutController.TimeoutException)
                {
                    Toast.makeText(ImageListWallActivity.this, R.string.error_timeout, Toast.LENGTH_LONG).show();
                }
                else if(error instanceof RetrofitError || error instanceof HttpException)
                {
                    Toast.makeText(ImageListWallActivity.this, R.string.error_fetch, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Provides items for our RecyclerView view.
     */
    class ImageListWallAdapter extends RecyclerView.Adapter<ImageListWallAdapter.ViewHolder>
    {
        private static final int TYPE_HEADER = 1;
        private static final int TYPE_ITEM = 2;
        private UserListingListViewModel data;

        public ImageListWallAdapter(UserListingListViewModel data)
        {
            this.data = data;
        }

        //TODO Need to add fade in effect
        private ImageDisplayOption getImageDisplayOption()
        {
            return new ImageDisplayOption.Builder().withImageResOnLoading(R.drawable.loading_thumbnail)
                                                   .withImageResForEmptyUri(R.drawable.ic_error)
                                                   .withImageResOnFail(R.drawable.ic_error)
                                                   .withResetViewBeforeLoading(true)
                                                   .withScaleType(ImageDisplayOption.IMAGE_SCALE_EXACTLY)
                                                   .withBitmapConfig(Bitmap.Config.RGB_565)
                                                   .withConsiderExifParams(true)
                                                   .build();
        }

        @Override
        public int getItemViewType(int position)
        {
            return position == 0 ? TYPE_HEADER : TYPE_ITEM;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
        {
            return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.imagelistwall_viewholder, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position)
        {

            final ViewHolder viewHolder = (ViewHolder) holder;
            final ImageWallListingViewModel imageWallListingViewModel = data.getImageWallListingViewModels().get(position);

            Date date = null;
            try
            {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(
                        imageWallListingViewModel.getCreatedAt().substring(0, imageWallListingViewModel.getCreatedAt().indexOf("T") + 1));
            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }
            String newstring = new SimpleDateFormat("dd-MM-yyyy").format(date);

            viewHolder.textViewDescription.setText("Created At : " + newstring + "  Likes " + imageWallListingViewModel.getLikes());

            viewHolder.textViewAuthorName.setText(imageWallListingViewModel.getUser().getUsername());

            viewHolder.textViewAbout.setText(imageWallListingViewModel.getCategories().get(0).getTitle());

            BaseApplication.getImageLoader()
                           .loadImage(imageWallListingViewModel.getUrls().getRegular(), viewHolder.imageViewHeader, getImageDisplayOption(),
                                      new AbstractImageLoadingListener()
                                      {
                                          @Override
                                          public void onLoadingStarted(String imageUri, View view)
                                          {
                                              LogUtil.printLog("", " imageList onLoadingStarted ");
                                              viewHolder.progressBarLoading.setVisibility(View.VISIBLE);
                                          }

                                          @Override
                                          public void onLoadingFailed(String imageUri, View view, Throwable failReason)
                                          {
                                              LogUtil.printLog("", " imageList onLoadingFailed ");
                                              viewHolder.progressBarLoading.setVisibility(View.GONE);
                                          }

                                          @Override
                                          public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
                                          {
                                              LogUtil.printLog("", " imageList onLoadingComplete ");
                                              viewHolder.progressBarLoading.setVisibility(View.GONE);
                                          }
                                      });
            BaseApplication.getImageLoader()
                           .loadImage(imageWallListingViewModel.getUser().getProfileImage().getMedium(), viewHolder.imageAuthor,
                                      getImageDisplayOption());

            // Create a system to run the physics loop for a set of springs.
            SpringSystem springSystem = SpringSystem.create();

            // Add a spring to the system.
            final Spring spring = springSystem.createSpring();

            viewHolder.imageViewLike.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            // When pressed start solving the spring to 1.
                            float mappedValue = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, 1, 0.5);
                            viewHolder.imageViewLike.setScaleX(mappedValue);
                            viewHolder.imageViewLike.setScaleY(mappedValue);
                            spring.setEndValue(1);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            // When released start solving the spring to 0.
                            mappedValue = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, 1, 0.5);
                            viewHolder.imageViewLike.setScaleX(mappedValue);
                            viewHolder.imageViewLike.setScaleY(mappedValue);
                            spring.setEndValue(0);
                            break;
                    }
                    return true;
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return data.getImageWallListingViewModels().size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder
        {
            protected CardView cardViewImageWallItem;
            protected ImageView imageViewHeader;
            protected ProgressBar progressBarLoading;
            protected TextView textViewDescription;
            protected TextView textViewAuthorName;
            protected ImageView imageViewLike;
            protected TextView textViewAbout;
            protected CircleImageView imageAuthor;

            public ViewHolder(View view)
            {
                super(view);

                cardViewImageWallItem = (CardView) view.findViewById(R.id.cardViewImageWallItem);
                imageAuthor = (CircleImageView) view.findViewById(R.id.imageAuthor);
                imageViewHeader = (ImageView) view.findViewById(R.id.imageViewHeader);
                textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
                progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarLoading);
                textViewAuthorName = (TextView) view.findViewById(R.id.textViewAuthorName);
                imageViewLike = (ImageView) view.findViewById(R.id.imageViewLike);
                textViewAbout = (TextView) view.findViewById(R.id.textViewAbout);
            }
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        recyclerViewImageWall.setLayoutManager(null);
    }
}

