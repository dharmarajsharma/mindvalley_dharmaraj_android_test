package com.test.www.mindvalley.viewmodel.userlisting;

/**
 * Created by dharmaraj on 13/07/2016.
 */

public class CategoryViewModel
{

        private Integer id;
        private String title;
        private Integer photoCount;
        private CategoryLinksViewModel categoryLinksViewModel;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The title
         */
        public String getTitle() {
            return title;
        }

        /**
         *
         * @param title
         * The title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         *
         * @return
         * The photoCount
         */
        public Integer getPhotoCount() {
            return photoCount;
        }

        /**
         *
         * @param photoCount
         * The photo_count
         */
        public void setPhotoCount(Integer photoCount) {
            this.photoCount = photoCount;
        }

        /**
         *
         * @return
         * The links
         */
        public CategoryLinksViewModel getLinks() {
            return categoryLinksViewModel;
        }

        /**
         *
         * @param links
         * The links
         */
        public void setLinks(CategoryLinksViewModel links) {
            this.categoryLinksViewModel = links;
        }

}
