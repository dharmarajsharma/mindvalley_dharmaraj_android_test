package com.test.www.mindvalley.network.userlisting.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by dharmaraj on 13/07/2016.
 */


public class ImageWallListingResponseNetworkModel
{
    private String id;
    @SerializedName("created_at")
    private String createdAt;
    private Integer width;
    private Integer height;
    private String color;
    private Integer likes;
    @SerializedName("liked_by_user")
    private Boolean likedByUser;
    private UserNetworkModel user;
    @SerializedName("current_user_collections")
    private List<Object> currentUserCollections = new ArrayList<Object>();
    @SerializedName("urls")
    private UrlsNetworkModel urls;
    @SerializedName("categories")
    private List<CategoryNetworkModel> categories = new ArrayList<CategoryNetworkModel>();
    @SerializedName("links")
    private LinkNetworkModel links;

    /**
     *
     * @return
     * The id
     */

    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */

    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The createdAt
     */

    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The width
     */

    public Integer getWidth() {
        return width;
    }

    /**
     *
     * @param width
     * The width
     */

    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     *
     * @return
     * The height
     */

    public Integer getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */

    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The color
     */

    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */

    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The likes
     */

    public Integer getLikes() {
        return likes;
    }

    /**
     *
     * @param likes
     * The likes
     */

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    /**
     *
     * @return
     * The likedByUser
     */

    public Boolean getLikedByUser() {
        return likedByUser;
    }

    /**
     *
     * @param likedByUser
     * The liked_by_user
     */

    public void setLikedByUser(Boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    /**
     *
     * @return
     * The user
     */

    public UserNetworkModel getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(UserNetworkModel user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The currentUserCollections
     */
    public List<Object> getCurrentUserCollections() {
        return currentUserCollections;
    }

    /**
     *
     * @param currentUserCollections
     * The current_user_collections
     */
    public void setCurrentUserCollections(List<Object> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    /**
     *
     * @return
     * The urls
     */

    public UrlsNetworkModel getUrls() {
        return urls;
    }

    /**
     *
     * @param urls
     * The urls
     */

    public void setUrls(UrlsNetworkModel urls) {
        this.urls = urls;
    }

    /**
     *
     * @return
     * The categories
     */

    public List<CategoryNetworkModel> getCategories() {
        return categories;
    }

    /**
     *
     * @param categories
     * The categories
     */
    public void setCategories(List<CategoryNetworkModel> categories) {
        this.categories = categories;
    }

    /**
     *
     * @return
     * The links
     */
    public LinkNetworkModel getLinks() {
        return links;
    }

    /**
     *
     * @param links
     * The links
     */
    public void setLinks(LinkNetworkModel links) {
        this.links = links;
    }

}
