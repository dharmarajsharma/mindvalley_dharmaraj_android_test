package com.test.www.mindvalley.network.userlisting.model;

import com.google.gson.annotations.SerializedName;
/**
 * Created by dharmaraj on 13/07/2016.
 */
public class UserNetworkModel
{
    private String id;
    private String username;
    private String name;
    @SerializedName("profile_image")
    private ProfileImageNetworkModel profileImage;
    private ImageListWallLinksNetworkModel links;

    /**
     * @return The id
     */

    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *         The id
     */

    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return The username
     */

    public String getUsername()
    {
        return username;
    }

    /**
     * @param username
     *         The username
     */

    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * @return The name
     */

    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *         The name
     */

    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return The profileImage
     */

    public ProfileImageNetworkModel getProfileImage()
    {
        return profileImage;
    }

    /**
     * @param profileImage
     *         The profile_image
     */

    public void setProfileImage(ProfileImageNetworkModel profileImage)
    {
        this.profileImage = profileImage;
    }

    /**
     * @return The links
     */

    public ImageListWallLinksNetworkModel getLinks()
    {
        return links;
    }

    /**
     * @param links
     *         The links
     */

    public void setLinks(ImageListWallLinksNetworkModel links)
    {
        this.links = links;
    }
}
