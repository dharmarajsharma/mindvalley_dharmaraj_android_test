package com.test.www.mindvalley;

import android.content.pm.PackageInfo;
import android.test.ApplicationTestCase;
import android.test.MoreAsserts;
import android.test.suitebuilder.annotation.SmallTest;

import com.test.www.mindvalley.application.ImageListWallApplication;

/**
 * Created by dharmaraj on 15/07/2016.
 */
public class ImageListWallApplicationTest extends ApplicationTestCase<ImageListWallApplication>
{

    private ImageListWallApplication application;

    public ImageListWallApplicationTest() {
        super(ImageListWallApplication.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        application = getApplication();

    }

    @SmallTest
    public void testCorrectVersion() throws Exception {
        PackageInfo info = application.getPackageManager().getPackageInfo(application.getPackageName(), 0);
        assertNotNull(info);
        MoreAsserts.assertMatchesRegex("\\d\\.\\d", info.versionName);
    }

}
