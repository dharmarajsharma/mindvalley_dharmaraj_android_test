package com.mindvalley.common.framework.mapper;


public interface IMapper<JsonModel, ViewModel>
{
    /**
     * A method used to convert the json network model to view model used by view.
     *
     * @param jsonModel
     *         network json model
     *
     * @return view model used by view.
     */
    ViewModel toViewModel(JsonModel jsonModel);
}
