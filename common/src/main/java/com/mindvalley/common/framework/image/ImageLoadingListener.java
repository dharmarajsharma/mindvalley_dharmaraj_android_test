package com.mindvalley.common.framework.image;

import android.graphics.Bitmap;
import android.view.View;
public interface ImageLoadingListener
{
    void onLoadingStarted(String uri, View view);

    void onLoadingFailed(String uri, View view, Throwable failureReason);

    void onLoadingComplete(String uri, View view, Bitmap bitmap);

    void onLoadingCancelled(String uri, View view);
}
