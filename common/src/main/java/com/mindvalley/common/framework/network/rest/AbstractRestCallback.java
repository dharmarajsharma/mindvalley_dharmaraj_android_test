package com.mindvalley.common.framework.network.rest;

import com.google.gson.Gson;
import com.mindvalley.common.framework.network.rest.model.RestError;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public abstract class AbstractRestCallback<T > implements Callback<T>
{
    private String body;

    public String getBody()
    {
        return body;
    }

    @Override
    public void success(T t, Response response)
    {
        if(t == null)
        {
            RestError error = new RestError();
            error.setStatus(false);
            error.setMessage("Please check the server response. We are getting null or empty response from server.");
            failure(error);
            return;
        }
        body = new Gson().toJson(t);
        success(t);
    }

    public abstract void success(T t);

    public abstract void failure(RestError error);

    @Override
    public void failure(RetrofitError error)
    {

        RestError restError = new RestError();

        restError.setStatus(false);
        restError.setMessage(error.getResponse().getStatus() + " " + error.getResponse().getReason());
        restError.setDetailMessage(error.getMessage());
        restError.setSuccessType(error.getSuccessType().toString());
        restError.setUrl(error.getUrl());
        if(error.getResponse().getBody() != null)
        {
            restError.setBody(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
        }
        failure(restError);
    }
}
