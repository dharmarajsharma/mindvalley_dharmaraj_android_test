package com.mindvalley.common.framework.viewmodel;

/**
 *  An interface for models used in a view class to show there data.
 *
 * Created by dharmaraj on 14/07/2016.
 */
public interface IViewModel
{
}
