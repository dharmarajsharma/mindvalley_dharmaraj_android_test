package com.mindvalley.common.framework.network.rest.model;

import org.parceler.Parcel;

@Parcel(parcelsIndex = false)
public final class RestError
{
    protected String url;
    protected String successType;
    protected String detailMessage;
    protected boolean status;
    protected String message;
    protected String body;

    public RestError()
    {
    }

    public RestError(String message)
    {
        this.message = message;
    }

    public boolean isStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getSuccessType()
    {
        return successType;
    }

    public void setSuccessType(String successType)
    {
        this.successType = successType;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }
}
