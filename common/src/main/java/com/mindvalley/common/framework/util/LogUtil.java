package com.mindvalley.common.framework.util;

import android.util.Log;

import com.mindvalley.common.framework.BuildConfig;

import java.io.PrintWriter;
import java.io.StringWriter;


public class LogUtil
{
    /**
     * Priority constant for the println method; use Log.v.
     */
    public static final int VERBOSE = Log.VERBOSE;

    /**
     * Priority constant for the println method; use Log.d.
     */
    public static final int DEBUG = Log.DEBUG;

    /**
     * Priority constant for the println method; use Log.i.
     */
    public static final int INFO = Log.INFO;

    /**
     * Priority constant for the println method; use Log.w.
     */
    public static final int WARN = Log.WARN;

    /**
     * Priority constant for the println method; use Log.e.
     */
    public static final int ERROR = Log.ERROR;

    private static final String TAG = "Vehicle Logs:";

    public static void printLog(Object msg)
    {
        printLog(TAG, msg);
    }

    public static void printLog(String tag, Object msg)
    {
        if(BuildConfig.DEBUG)
        {
            Log.e(tag, msg.toString());
        }
    }

    public static void printLog(Object msg, Throwable exception)
    {
        printLog(TAG, msg, exception);
    }

    public static void printLog(String tag, Object msg, Throwable exception)
    {
        if(BuildConfig.DEBUG)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(tag).append(", Message: ").append(msg.toString());
            StringWriter writer = new StringWriter();
            exception.printStackTrace(new PrintWriter(writer));
            builder.append(", Exception: ").append(writer.toString());
            Log.e(TAG, builder.toString());
        }
    }

    public static void debugLog(int logType, Object msg)
    {
        debugLog(logType, TAG, msg);
    }

    /**
     * Method is used to Print Log
     * 0 - Log.d, 1 - Log.e, 2 - Log.i, 3 - Log.v, 4 - Log.w
     * default - Log.d
     *
     * @param LogType
     * @param tag
     * @param msg
     */
    public static void debugLog(int LogType, String tag, Object msg)
    {
        if(BuildConfig.DEBUG)
        {
            switch(LogType)
            {
                case DEBUG:
                    Log.d(tag, msg.toString());
                    break;
                case ERROR:
                    Log.e(tag, msg.toString());
                    break;
                case INFO:
                    Log.i(tag, msg.toString());
                    break;
                case VERBOSE:
                    Log.v(tag, msg.toString());
                    break;
                case WARN:
                    Log.w(tag, msg.toString());
                    break;
                default:
                    Log.d(tag, msg.toString());
                    break;
            }
        }
    }
}
