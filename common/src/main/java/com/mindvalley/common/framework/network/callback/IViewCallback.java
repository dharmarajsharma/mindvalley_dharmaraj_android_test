package com.mindvalley.common.framework.network.callback;

import com.mindvalley.common.framework.viewmodel.IViewModel;

/**
 * This callback is used to send the message to the view from network with underlying view model, which is required to show the data on
 * view.
 *
 * @param <T>
 *         T must be extends with IViewModel, which is further used by the views to show the content.
 *
 */
public interface IViewCallback<T extends IViewModel>
{
    /**
     * Called when network call get response as a success.
     *
     * @param t
     *         view model object for the view
     */
    void onSuccess(T t);

    /**
     * * Called when network call get response as a failure.
     *
     * @param e
     *         throwable object
     */
    void onFailure(Throwable e);
}
