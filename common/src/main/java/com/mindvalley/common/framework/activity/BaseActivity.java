package com.mindvalley.common.framework.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mindvalley.common.framework.application.BaseApplication;
import com.mindvalley.common.framework.navigation.Navigator;


/**
 * Created by dharmaraj on 14/07/2016.
 */
public abstract class BaseActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        onPreSetContentView();

        setContentView(getActivityLayout());

        initViews();

        onActivityReady();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    protected Navigator getNavigator()
    {
        return ((BaseApplication) getApplication()).getNavigator();
    }

    protected abstract void onPreSetContentView();

    protected abstract void onActivityReady();

    protected abstract int getActivityLayout();

    public abstract String getScreenName();

    protected abstract void initViews();
}
