package com.mindvalley.common.framework.application;

import android.app.Application;
import android.os.Process;
import com.mindvalley.common.framework.image.ImageLoader;
import com.mindvalley.common.framework.image.UniversalImageLoader;
import com.mindvalley.common.framework.navigation.Navigator;

/**
 * Created by dharmaraj on 14/07/2016.
 */
public abstract class BaseApplication extends Application
{
    private static ImageLoader imageLoader;

    public static ImageLoader getImageLoader()
    {
        return imageLoader;
    }

    public abstract Navigator getNavigator();

    @Override
    public void onCreate()
    {
        super.onCreate();

        isApplicationInstalled();
        imageLoader = new UniversalImageLoader(this);
        onPostCreate();
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();
    }


    public abstract void onPostCreate();

    /**
     * Checks if application installed properly or not.
     */
    private void isApplicationInstalled()
    {
        if(getResources() == null)
        {
            Process.killProcess(Process.myPid());
        }
    }
}
