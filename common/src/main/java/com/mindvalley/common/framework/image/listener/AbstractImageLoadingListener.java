package com.mindvalley.common.framework.image.listener;

import android.graphics.Bitmap;
import android.view.View;

import com.mindvalley.common.framework.image.ImageLoadingListener;

public abstract class AbstractImageLoadingListener implements ImageLoadingListener
{
    @Override
    public void onLoadingStarted(String imageUri, View view)
    {
    }

    @Override
    public void onLoadingFailed(String uri, View view, Throwable failureReason)
    {
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
    {
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view)
    {
    }
}
