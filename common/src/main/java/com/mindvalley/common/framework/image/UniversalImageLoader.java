package com.mindvalley.common.framework.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.mindvalley.common.framework.BuildConfig;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

public class UniversalImageLoader implements ImageLoader
{
    private static final int MB = 1024 * 1024;
    private static final int MEMORY_CACHE_SIZE = (int) 1.5 * MB;
    private static final int DISK_CACHE_SIZE = 5 * MB;

    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;

    public UniversalImageLoader(Context context)
    {
        //caching for images enable for memory only
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(false).build();
        File cacheDir = StorageUtils.getCacheDirectory(context);
        ImageLoaderConfiguration.Builder configBuilder = new ImageLoaderConfiguration.Builder(context);
        configBuilder.threadPriority(1);
        if(BuildConfig.DEBUG)
        {
            configBuilder.writeDebugLogs();
        }
        configBuilder.denyCacheImageMultipleSizesInMemory();
        configBuilder.defaultDisplayImageOptions(defaultOptions);
        configBuilder.memoryCache(new LruMemoryCache(MEMORY_CACHE_SIZE));
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        imageLoader.init(configBuilder.build());
        imageLoader.handleSlowNetwork(true);
    }

    @Override
    public void loadImage(String uri, final ImageLoadingListener listener)
    {
        com.nostra13.universalimageloader.core.listener.ImageLoadingListener listener1 = null;
        if(listener != null)
        {
            listener1 = new com.nostra13.universalimageloader.core.listener.ImageLoadingListener()
            {
                @Override
                public void onLoadingStarted(String s, View view)
                {
                    listener.onLoadingStarted(s, view);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason)
                {
                    listener.onLoadingFailed(s, view, failReason.getCause());
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap)
                {
                    listener.onLoadingComplete(s, view, bitmap);
                }

                @Override
                public void onLoadingCancelled(String s, View view)
                {
                    listener.onLoadingCancelled(s, view);
                }
            };
        }

        imageLoader.loadImage(uri, listener1);
    }

    @Override
    public void loadImage(String uri, ImageView imageView)
    {
        imageLoader.displayImage(uri, imageView);
    }

    @Override
    public void loadImage(String uri, ImageView imageView, ImageDisplayOption options)
    {
        DisplayImageOptions options1 = null;
        if(options != null)
        {
            DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder().showImageForEmptyUri(options.getImageResForEmptyUri())
                                                                                   .showImageOnFail(options.getImageResOnFail())
                                                                                   .showImageOnLoading(options.getImageResOnLoading())
                                                                                   .considerExifParams(options.isConsiderExifParams())
                                                                                   .resetViewBeforeLoading(
                                                                                           options.isResetViewBeforeLoading())
                                                                                   .delayBeforeLoading(options.getDelayBeforeLoading())
                                                                                   .cacheInMemory(true)
                                                                                   .cacheOnDisk(false);
            if(options.getBitmapConfig() != null)
            {
                builder.bitmapConfig(options.getBitmapConfig());
            }
            if(options.getScaleType() != null)
            {
                if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_NONE))
                {
                    builder.imageScaleType(ImageScaleType.NONE);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_NONE_SAFE))
                {
                    builder.imageScaleType(ImageScaleType.NONE_SAFE);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_EXACTLY))
                {
                    builder.imageScaleType(ImageScaleType.EXACTLY);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_EXACTLY_STRETCHED))
                {
                    builder.imageScaleType(ImageScaleType.EXACTLY_STRETCHED);
                }
            }
            options1 = builder.build();
        }
        imageLoader.displayImage(uri, imageView, options1);
    }

    @Override
    public void loadImage(String uri, ImageView imageView, ImageDisplayOption options, final ImageLoadingListener listener)
    {
        DisplayImageOptions options1 = null;
        if(options != null)
        {
            DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder().showImageForEmptyUri(options.getImageResForEmptyUri())
                                                                                   .showImageOnFail(options.getImageResOnFail())
                                                                                   .showImageOnLoading(options.getImageResOnLoading())
                                                                                   .considerExifParams(options.isConsiderExifParams())
                                                                                   .resetViewBeforeLoading(
                                                                                           options.isResetViewBeforeLoading())
                                                                                   .delayBeforeLoading(options.getDelayBeforeLoading())
                                                                                   .cacheInMemory(true)
                                                                                   .cacheOnDisk(false);
            if(options.getBitmapConfig() != null)
            {
                builder.bitmapConfig(options.getBitmapConfig());
            }
            if(options.getScaleType() != null)
            {

                if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_NONE))
                {
                    builder.imageScaleType(ImageScaleType.NONE);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_NONE_SAFE))
                {
                    builder.imageScaleType(ImageScaleType.NONE_SAFE);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_EXACTLY))
                {
                    builder.imageScaleType(ImageScaleType.EXACTLY);
                }
                else if(options.getScaleType().equals(ImageDisplayOption.IMAGE_SCALE_EXACTLY_STRETCHED))
                {
                    builder.imageScaleType(ImageScaleType.EXACTLY_STRETCHED);
                }
            }
            options1 = builder.build();
        }
        com.nostra13.universalimageloader.core.listener.ImageLoadingListener listener1 = null;
        if(listener != null)
        {
            listener1 = new com.nostra13.universalimageloader.core.listener.ImageLoadingListener()
            {
                @Override
                public void onLoadingStarted(String s, View view)
                {
                    listener.onLoadingStarted(s, view);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason)
                {
                    listener.onLoadingFailed(s, view, failReason.getCause());
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap)
                {
                    listener.onLoadingComplete(s, view, bitmap);
                }

                @Override
                public void onLoadingCancelled(String s, View view)
                {
                    listener.onLoadingCancelled(s, view);
                }
            };
        }
        imageLoader.displayImage(uri, imageView, options1, listener1);
    }
}
