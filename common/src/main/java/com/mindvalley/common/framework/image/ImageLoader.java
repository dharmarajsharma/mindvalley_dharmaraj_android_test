package com.mindvalley.common.framework.image;

import android.widget.ImageView;
public interface ImageLoader
{
    void loadImage(String uri, ImageLoadingListener listener);

    void loadImage(String uri, ImageView imageView);

    void loadImage(String uri, ImageView imageView, ImageDisplayOption options);

    void loadImage(String uri, ImageView imageView, ImageDisplayOption options, ImageLoadingListener listener);
}
