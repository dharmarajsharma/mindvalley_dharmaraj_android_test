package com.mindvalley.common.framework.network.rest;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mindvalley.common.framework.network.rest.interceptor.CacheInterceptor;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public abstract class AbstractRestClient
{
    private static final long CACHE_SIZE = 2 * 1024 * 1024; //2MB
    protected RestAdapter restAdapter;

    public AbstractRestClient(final Context context, final String baseUrl, boolean debug)
    {
        Gson gson = new GsonBuilder().setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'").create();

        //cache for url content is in memory only
        Cache cache = new Cache(new File(context.getCacheDir(), "http"), CACHE_SIZE);

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setCache(cache);
        okHttpClient.setReadTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(90, TimeUnit.SECONDS);

        CacheInterceptor cInterceptor = new CacheInterceptor(context);
        okHttpClient.networkInterceptors().add(cInterceptor);
        okHttpClient.interceptors().add(cInterceptor);

        RestAdapter.Builder builder = new RestAdapter.Builder();
        if(debug)
        {
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
        }
        builder.setEndpoint(baseUrl);
        builder.setConverter(new GsonConverter(gson));
        builder.setClient(new OkClient(okHttpClient));
        restAdapter = builder.build();

        initApi();
    }

    public abstract void initApi();
}
