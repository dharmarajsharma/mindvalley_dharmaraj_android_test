package com.mindvalley.common.framework.view.animator;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ScaleInRecyclerViewAdapter extends RecyclerViewAdapterAnimator
{
    private static final float DEFAULT_SCALE_FROM = .5f;
    private final float mFrom;
    private boolean showAnimation = true;

    public ScaleInRecyclerViewAdapter(RecyclerView.Adapter adapter)
    {
        this(adapter, DEFAULT_SCALE_FROM);
    }

    public ScaleInRecyclerViewAdapter(RecyclerView.Adapter adapter, boolean showAnimation)
    {
        this(adapter, DEFAULT_SCALE_FROM);
        this.showAnimation = showAnimation;
    }

    public ScaleInRecyclerViewAdapter(RecyclerView.Adapter adapter, float from)
    {
        super(adapter);
        mFrom = from;
    }

    public boolean isShowAnimation()
    {
        return showAnimation;
    }

    public void setShowAnimation(boolean showAnimation)
    {
        this.showAnimation = showAnimation;
    }

    @Override
    protected Animator[] getAnimators(View view)
    {
        if(showAnimation)
        {
            ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
            ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
            return new ObjectAnimator[]{scaleX, scaleY};
        }
        return null;
    }
}