package com.mindvalley.common.framework.network.rest.interceptor;

import android.content.Context;

import com.mindvalley.common.framework.util.NetworkUtils;
import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class CacheInterceptor implements Interceptor
{
    private Context context;

    public CacheInterceptor(Context context)
    {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request originalRequest = chain.request();
        Request.Builder request = originalRequest.newBuilder();
        if(originalRequest.header("fresh") != null)
        {
            request.cacheControl(CacheControl.FORCE_NETWORK);
        }
        Response response = chain.proceed(request.build());
        return response.newBuilder().removeHeader("Pragma").removeHeader("Cache-Control").header("Cache-Control", cacheControl()).build();
    }

    private String cacheControl()
    {
        String cacheHeader;
        if(NetworkUtils.isNetworkAvailable(context))
        {
            cacheHeader = "public, max-age=2419200";
        }
        else
        {
            cacheHeader = "public, only-if-cached, max-stale=2419200";
        }
        return cacheHeader;
    }
}
