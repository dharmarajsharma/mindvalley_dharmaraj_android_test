package com.mindvalley.common.framework.util;

import android.app.Activity;

import com.mindvalley.common.framework.R;

public class AnimationUtil
{
    /**
     * @param activity
     *         called activity ..
     *
     * @return
     */
    public static void setTransactionAnimationFromCalledActivity(Activity activity)
    {
        activity.overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
    }

    /**
     * @param activity
     *         caller activity..
     *
     * @return
     */
    public static void setTransactionAnimationFromCallerActivity(Activity activity)
    {
        activity.overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
    }
}
