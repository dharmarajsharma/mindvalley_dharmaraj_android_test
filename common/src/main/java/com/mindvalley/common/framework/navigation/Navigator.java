package com.mindvalley.common.framework.navigation;

import android.content.Context;

public abstract class Navigator
{
    public Navigator()
    {
    }

    public abstract void launchUserListingActivity(Context context);


}
