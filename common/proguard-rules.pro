# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Programs/Android/SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes *Annotation*,Signature,Exceptions,*InnerClasses*
-keep public interface com.mindvalley.common.common.viewmodel.IViewModel
-keep public interface com.mindvalley.common.common.mapper.IMapper { *; }
-keep public class com.mindvalley.common.common.preference.AbstractPreferenceManager { *; }
-keep public class com.mindvalley.common.common.util.** { *; }
-keep public class com.mindvalley.common.common.cache.CacheManager { *; }
-keep public class com.mindvalley.common.common.cache.CacheManager$CacheType { *; }

-keep public class com.mindvalley.common.common.network.api.IApiServiceFactory { *; }
-keep public class com.mindvalley.common.common.network.callback.IViewCallback { *; }
-keep public class com.mindvalley.common.common.network.config.INetworkConfiguration { *; }
-keep public class com.mindvalley.common.common.network.locator.IServiceLocator { *; }
-keep public class com.mindvalley.common.common.network.service.IService
-keep public class com.mindvalley.common.common.network.service.IServiceFactory { *; }

-keep public class com.mindvalley.common.common.newcarlead.** { *; }